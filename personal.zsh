# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

# For example: add yourself some shortcuts to projects you often work on.
#
# brainstormr=~/Projects/development/planetargon/brainstormr
# cd $brainstormr
#

# Weather
function weather(){ curl wttr.in/"$@"; }


# Frequent Aliases
alias gs="git status"
alias gsh='git shorty'
alias gaa='git add .'
alias ga='git add'
alias gcln='git clean -dfx'

alias zzz='pmset sleepnow'
alias slime='open -a "Sublime Text"'
alias rezsh='source ~/.zshrc'

# Requires the homebrew package exa
alias exat='exa -T -L 2'
alias exatt='exa -T -L 3'
alias exattt='exa -T -L 4'
alias exatttt='exa -T'
alias lx='exa -al'

# Bainbridge Specific Functions
function tagdate {
    commit_count=0
    git tag "$(date +'%Y%m%d').$commit_count"
    while [ $? -ne 0 ]
    do
        (( commit_count ++ ))
        git tag "$(date +'%Y%m%d').$commit_count"
    done
    echo "Tagged $(date +'%Y%m%d').$commit_count"
}

function deploy_to_dev {
    git tag -f dev
    git push -f origin dev
    phb deploy bbh-platform --kube-ctx=bbh-dev --config-env=dev --kube-ns=bbh-dev
}

function deploy_to_qa {
    vared -p "Are you sure you want to push to qa? " -c reply
    echo
    if [[ $reply =~ ^[Yy]$ ]]
    then
        git tag -f qa
        git push -f origin qa
        phb deploy bbh-platform --kube-ctx=bbh-dev --config-env=qa --kube-ns=bbh-qa
    else
        echo "Push cancelled."
    fi
}

function deploy_to_prod {
    vared -p "Are you sure you want to push to prod? " -c reply
    echo    # (optional) move to a new line
    if [[ $reply =~ ^[Yy]$ ]]
    then
        git tag -f prod
        git push -f origin prod
        phb deploy bbh-platform --kube-ctx=bbh-prod --config-env=prod --kube-ns=bbh-prod
    else
        echo "Push cancelled."
    fi
}

# Git credentials
git config --global user.name "Mikayla Thompson"
git config --global user.email "mikaylathompson@bainbridgehealth.com"
