# Homebrew
echo "Checking if homebrew is installed."
which -s brew
if [[ $? != 0 ]] ; then
    # Install Homebrew
    echo "\nNope, installing now."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
    brew update
fi

# Zsh
if [ "$SHELL" != "/usr/bin/zsh" ]; then
	echo "\nInstalling zsh"
	brew install zsh
	echo "\nSetting zsh as default shell"
	# sudo chsh -s /usr/bin/zsh $USER
    sudo chsh -s /usr/local/bin/zsh $USER
	echo "\nMay need to log out and back in before continuing."
fi

# Oh-my-zsh
if [ ! -d ~/.oh-my-zsh ]; then
	echo "\nInstalling oh-my-zsh"
    if brew ls --versions myformula > /dev/null; then
        echo "Wget already installed."
    else
        echo "Installing wget"
        brew install wget
    fi
	sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
fi

# Vundle
if [ ! -d ~/.vim/bundle/ ]; then
	echo "\nInstalling Vundle"
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
echo "\nInsalling Vim plugins"
vim +PluginInstall +qall

# Save current .zshrc and .vimrc files and write new, blank-ish ones
if [ -f "~/.zshrc" ]; then
    mv ~/.zshrc "~/.$(date +'%Y%m%d').zshrc"
fi

echo "source ~/dotfiles/.zshrc" > ~/.zshrc

if [ -f "~/.vimrc" ]; then
    mv ~/.vimrc "~/.$(date +'%Y%m%d').vimrc"
fi
echo "source ~/dotfiles/.vimrc" > ~/.vimrc

echo "source ~/dotfiles/personal.zsh" > ~/.oh-my-zsh/custom/personal.zsh

echo "Applying zsh changes"
source ~/.zshrc

echo "Setting mac default behaviors"
source ~/dotfiles/.osx

