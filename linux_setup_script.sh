# Zsh
if [ "$SHELL" != "/usr/bin/zsh" ]; then
	echo "\nInstalling zsh"
	sudo apt-get update && sudo apt-get install zsh
	echo "\nSetting zsh as default shell"
	sudo chsh -s /usr/bin/zsh $USER
	echo "\nMay need to log out and back in before continuing."
fi

# Oh-my-zsh
if [ ! -d ~/.oh-my-zsh ]; then
	echo "\nInstalling oh-my-zsh"
	sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
fi

# Vundle
if [ ! -d ~/.vim/bundle/ ]; then
	echo "\nInstalling Vundle"
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
echo "\nInsalling Vim plugins"
vim +PluginInstall +qall

# Save current .zshrc and .vimrc files and write new, blank-ish ones
mv ~/.zshrc "~/.$(date +'%Y%m%d').zshrc"
echo "source ~/dotfiles/.zshrc" > ~/.zshrc

mv ~/.vimrc "~/.$(date +'%Y%m%d').vimrc"
echo "source ~/dotfiles/.vimrc" > ~/.vimrc


# Run dev setup functions
# This is from https://gist.githubusercontent.com/timothyhahn/32b18cd259624218275d665813d3d0dc/raw/80ecf21d79450c90d72e55911a79d9ab2fde3379/returntheslab.sh

# Update and install inital req's
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common python-pip git golang libseccomp-dev wget

# Install Docker
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce

# Add $USER to docker
sudo usermod -aG docker $USER

# Fix https://bugs.chromium.org/p/chromium/issues/detail?id=860565
# Thanks to https://bugs.chromium.org/p/chromium/issues/detail?id=860565

export GOPATH=~/go

go get github.com/opencontainers/runc
cd ~/go/src/github.com/opencontainers/runc

git remote add tmp https://github.com/AkihiroSuda/runc.git
git fetch tmp
git -c "user.name=meh" -c "user.email=nope@naw.fam" merge  tmp/decompose-rootless-pr -m "Merging rootless mode disable fix"

make

sudo cp runc /usr/local/bin/runc-crostini

sudo tee -a /etc/docker/daemon.json <<EOF
{
  "runtimes": {
    "runc-crostini": {
      "path": "/usr/local/bin/runc-crostini"
    }
  },
  "default-runtime": "runc-crostini"
}
EOF

sudo service docker restart

echo "Feel free to delete $GOPATH if you'd like"

# Install Helm
wget https://storage.googleapis.com/kubernetes-helm/helm-v2.11.0-linux-amd64.tar.gz
tar xvf helm-v2.11.0-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/bin
rm -rf helm-v2.11.0-linux-amd64.tar.gz linux-amd64/

# Install & configure gcloud
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install google-cloud-sdk
sudo apt-get update && sudo apt-get install google-cloud-sdk kubectl
sudo apt-get -y install google-cloud-sdk kubectl

echo "Init-ting gcloud, please follow the prompts"
gcloud init

gcloud auth application-default login
gcloud auth configure-docker
