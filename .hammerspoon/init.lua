hs.hotkey.bind({"cmd", "alt", "ctrl"}, "W", function()
    hs.alert.show("Hello, World!")
    -- hs.notify.new({title="Hammerspoon", informativeText="Hello, world."}):send()
end)

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "R", function()
    hs.reload()
end)

function visual_beep(name)
    local style = {fillColor = hs.drawing.color.hammerspoon.osx_red,
                    textSize = 50}
    hs.alert.show("BEEP", 1, style)
end

beep_watcher = hs.distributednotifications.new(visual_beep, "com.apple.systemBeep")
beep_watcher:start()


dofile("anycomplete.lua")

dofile("switcher.lua")

hs.caffeinate.watcher.new(daze_runner):start()
hs.alert.show("Config loaded.")

