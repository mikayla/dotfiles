
chooser_active = false
n_rotations = 0

function end_app_chooser(selection)
    chooser_active = false
    if not selection then return end

    if selection.isWindow then
        hs.window.get(selection.windowid):focus()
    else
        hs.application.find(selection.bundleid):activate()
    end
end

function get_available_apps()
    -- local all_apps = hs.application.find()
    all_windows = {}
    for i, window in ipairs(hs.window.orderedWindows()) do
        -- print(window)
        local entry = {text = window:application():name(),
                        subText = window:title(),
                        windowid = window:id(),
                        isWindow = true,
                        -- image = window:snapshot()}
                        image = hs.image.imageFromAppBundle(window:application():bundleID())}
        table.insert(all_windows, entry)
    end

    for i, app in ipairs(hs.application.runningApplications()) do
        if app:kind() == 1 and next(app:visibleWindows()) == nil then
            local entry = {text = app:title(),
                            isWindow = false,
                            bundleid = app:bundleID(),
                            image = hs.image.imageFromAppBundle(app:bundleID())}
            table.insert(all_windows, entry)
        end
    end

    return all_windows
end

function rotate(tbl, rotate_distance)
    rotate_distance = rotate_distance % #tbl
    local leftovers_table = {}
    for i = 1, rotate_distance, 1 do
        table.insert(leftovers_table, tbl[1])
        table.remove(tbl, 1)
    end

    for k, v in ipairs(leftovers_table) do
        table.insert(tbl, v)
    end
    return tbl
end

function show_app_chooser()
    if chooser_active then
        hs.eventtap.keyStroke({}, "down")
        print(chooser_active:selectedRow())
        -- hs.eventtap.keyStrokes("helloworld")
        hs.eventtap.keyStroke({"cmd"}, "1")
        -- n_rotations = n_rotations + 1
        -- chooser_active:choices(rotate(get_available_apps(), n_rotations))
        return
    end
    local chooser = hs.chooser.new(end_app_chooser)
    chooser:choices(get_available_apps)
    chooser:searchSubText(true)
    chooser:rows(15)
    chooser:show()
    chooser_active = chooser
    n_rotations = 0
end


-- mods, key, message, pressedfn, releasedfn, repeatfn
hs.hotkey.bind({"alt"}, "tab", show_app_chooser)
