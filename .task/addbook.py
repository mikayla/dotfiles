import subprocess
from sys import exit

import requests
import json
import xml.etree.ElementTree as ET
from pathlib import Path


def get_title():
    title = input('Title: ')
    return title

def get_author():
    author = input('Author: ')
    return author

def get_genre():
    genres = {'f': 'fiction', 'n': 'non-fiction', 'o': 'other'}
    genre = input('[f]iction, [n]on-fiction, [o]other? ')
    if genre in genres.keys():
        return genres[genre]
    else:
        print("Not an allowed choice. Try again.")
        return get_genre()


def get_author_with_suggestions(title):
    # Make a goodreads api request
    key = json.load(Path('/Users/mikayla/.task/goodreads.json').open())['key']
    url = "https://www.goodreads.com/search/index.xml"
    payload = {"key": key, "q": title, "search": "title"}
    try:
        response = requests.request("GET", url, data=payload)
        root = ET.fromstring(response.text)
        books = {b.find('./best_book/author/name').text : (b.find('./ratings_count').text, b.find('./best_book/title').text)
                for b in root.findall('./search/results/work')[::-1]}
        authors = sorted(list(books.keys()), key=lambda x: int(books[x][0]), reverse=True)[0:5]
    except:
        return (title, get_author())

    for i, auth in enumerate(authors):
        if books[auth][1] != title:
            print("[{}] {} by {}".format(i, books[auth][1], auth))
        else:
            print("[{}] {}".format(i, auth))
    print("[5] Other")

    author_num = input("Select author: ")
    if author_num == '':
        exit(0)
    elif author_num == '5':
        author = get_author()
    else:
        author = authors[int(author_num)]
        title = books[author][1]
    return (title, author)


def advanced_workflow():
    title = get_title()
    author = get_author_with_suggestions(title)


if __name__ == "__main__":
    title = get_title()
    if title == '':
        exit(0) 
    # author = get_author()
    title, author = get_author_with_suggestions(title)
    if author == '':
        exit(0)
    genre = get_genre()
    desc = "{} by {}".format(title, author)
    cmds = ' '.join(["task", "add", "tag:book",
            'author:"{}"'.format(author),
            'title:"{}"'.format(title),
            'genre:"{}"'.format(genre),
            'description:"{}"'.format(desc)])
    subprocess.run(cmds, shell=True, check=True)

