if has("gui_macvim")
    macmenu &File.New\ Tab key=<nop>
    map <D-t> <Plug>(CommandT)

    macmenu &Tools.Make key=<nop>
    map <D-b> <Plug>(CommandTBuffer)

    macmenu &File.Close key=<nop>
    map <D-w> :bp\|bd #<CR>
endif

nnoremap <silent> <Leader>t <Plug>(CommandT)
nnoremap <silent> <Leader>b <Plug>(CommandTBuffer)
nnoremap <silent> <Leader>j <Plug>(CommandTJump)
