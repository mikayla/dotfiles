set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'tpope/vim-sensible'
Plugin 'bling/vim-airline'
Plugin 'scrooloose/syntastic'
Plugin 'fholgado/minibufexpl.vim'
Plugin 'ervandew/supertab'
Plugin 'wincent/command-t'
Plugin 'scrooloose/nerdtree'
Plugin 'rust-lang/rust.vim'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-commentary'
Plugin 'airblade/vim-gitgutter'

call vundle#end()            " required
filetype plugin indent on    " required

syntax on  " Turn on syntax highlighting

" Indent settings: should be 4 space
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" Will autoformat rust files
let g:rustfmt_autosave = 1

" Settings for syntastic
" let g:syntastic_cpp_no_include_search = 1
" let g:syntastic_cpp_remove_include_errors = 1
let g:syntastic_quiet_messages = {
        \'regex': 'file not found'}
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

set laststatus=2

set foldmethod=indent

set number   " Adds line numbers along the left
highlight LineNr ctermfg=Grey

" Open NERDTree with ctrl-n
map <C-n> :NERDTreeToggle<CR>

" To implement updates: :so ~/.vimrc
